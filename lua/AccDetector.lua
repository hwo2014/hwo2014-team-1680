AccDetector = 
{
	speedDetector	= false,
	currentAcc		= 0,

	_prevSpeed		= 0,
	_isOnStraight	= false,
}

AccDetector.__index = AccDetector

function AccDetector:OnLineStart()
	self._isOnStraight = true
end

function AccDetector:OnLineEnd()
	self._isOnStraight = false
end

function AccDetector:Update()
	if self.speedDetector.currentSpeed == self._prevSpeed then
		return
	end
	self.currentAcc = self.speedDetector.currentSpeed - self._prevSpeed
	self._prevSpeed = self.speedDetector.currentSpeed
end
