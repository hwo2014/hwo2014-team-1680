function CreateObject(source)
	local obj = {}
	setmetatable(obj, source)
	return obj
end

function FloatMap(_left, _right, _val, _outLeft, _outRight)
	return (_val - _left) / (_right - _left) * (_outRight - _outLeft) + _outLeft
end

function FloatClamp(_val, _min, _max)
	if _val < _min then
		return _min
	end
	if _val > _max then
		return _max
	end
	return _val
end
