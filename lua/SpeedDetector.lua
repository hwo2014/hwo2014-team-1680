SpeedDetector = 
{
	gameData		= false,
	currentSpeed	= 0,
	bot				= false,

	_lastPos	= false,
	_lastTick	= 0,
}

SpeedDetector.__index = SpeedDetector

-- function SpeedDetector.create()
-- 	local obj = {}
-- 	setmetatable(obj, SpeedDetector)
-- 	return obj
-- end

function SpeedDetector:OnCarPosition(_carInfo, _tick)
	local currentPos = _carInfo.piecePosition
	if not self._lastPos then
		-- first tick
		self._lastPos	= currentPos
		self._lastTick	= _tick
		return
	end

	if currentPos.pieceIndex ~= self._lastPos.pieceIndex then
		-- new piece
		local prevPieceIndex = _carInfo.piecePosition.pieceIndex - 1
		if prevPieceIndex == -1 then
			prevPieceIndex = #self.gameData.track.pieces - 1
		end
		-- in lua, index starts from 1, while prevPieceIndex starts from 0
		local piece = self.gameData.track.pieces[prevPieceIndex + 1]
		local pieceLenght = bot:GetLaneLength(prevPieceIndex, bot._currentLane.startLaneIndex)
		dist = pieceLenght - self._lastPos.inPieceDistance + currentPos.inPieceDistance
		if dist < self.currentSpeed - 1 then
			print("switched; TODO get switch piece length")
			dist = self.currentSpeed
		end
		print("new piece, dist "..dist)
		-- print(self._lastPos.inPieceDistance)
		-- print(self._lastPos.lane.startLaneIndex)
		-- print(self._lastPos.lane.endLaneIndex)
		print("old pos "..self._lastPos.inPieceDistance.." in lane "..self._lastPos.lane.startLaneIndex.." out lane "..self._lastPos.lane.endLaneIndex)
	else
		dist = currentPos.inPieceDistance - self._lastPos.inPieceDistance
	end
	
	self.currentSpeed = dist / (_tick - self._lastTick)

	if _tick > self._lastTick + 1 then
		print("*************Skipped a tick")
	end

	self._lastPos	= currentPos
	self._lastTick	= _tick
end
