local socket = require "socket"
local json = require "json"
require "Helpers"
require "SpeedDetector"
require "AccDetector"

local DEBUG = false

local function LOGD(msg, params)
if DEBUG then
  print(string.format(msg, params))
end
end

local NoobBot =
{
	_gameData		= false,
	_pieceIndex		= 0,
	_accToSend		= 1,
	_currentTick	= 0,
	_numPieces		= false,
	_lastAngle		= 0,
	_currentLane	= false,
	_isOnSwitch		= false,
	_prevAngle		= 0,
	_prevAngleSpeed	= 0,
	_prevSpeed		= 0,

	_targetSpeed	= 100,
	_newTargetSpeed	= 100,
	_gotClose		= false,

	_turboDuration	= 0,
	_turboFactor	= 0,

	---------- CONSTANTS ------------
	cDecelerationConstant	= 0.02,
	cAccAmplitudeFactor		= 1.5,
	cDecelAmplitudeFactor	= 1.8,
	cMaxSpeed = 1000,

	-- debug stuff
	_target = 7.4,
	_reached = false,
	_distFromTarget = 0,

	_test = false,
}

NoobBot.__index = NoobBot

function NoobBot.create(conn, name, key)
	local bot = {}
	setmetatable(bot, NoobBot)
	bot.conn = conn
	bot.name = name
	bot.key = key

	-- init stuff
	print("Creating stuff")
	bot._speedDetector	= CreateObject(SpeedDetector)
	bot._accDetector	= CreateObject(AccDetector)
	bot._accDetector.speedDetector = bot._speedDetector

	return bot
end

	------------------------- ACTIONS ---------------------------
function NoobBot:msg(msgtype, data)
	return json.encode.encode({msgType = msgtype, data = data})
end

function NoobBot:send(msg)
	LOGD("Sending msg: %s", msg)
	self.conn:send(msg .. "\n")
end

function NoobBot:join()
	return self:msg("join", {name = self.name, key = self.key})
end

function NoobBot:throttle(throttle)
	return self:msg("throttle", throttle)
end

function NoobBot:ping()
	return self:msg("ping", {})
end

function NoobBot:run()
	self:send(self:join())
	self:msgloop()
end

function NoobBot:SwitchLane(_sLane)
	--print("switching lane to " .. _sLane)
	self:send(self:msg("switchLane", _sLane))
end

function NoobBot:ConsiderSwitch()
	local laneLenghts = {0, 0, 0}
	--print("Considering switch from lane " .. self._currentLane.endLaneIndex .. " on piece " .. self._pieceIndex)
	for offset = 2, 1000 do
		local pieceIndex = (self._pieceIndex + offset) % self._numPieces
		if self._gameData.track.pieces[pieceIndex + 1].switch then
			break
		end
		--print(offset)
		for laneOffset = -1, 1 do
			local laneIndex = self._currentLane.endLaneIndex + laneOffset
			if laneIndex >= 0 and laneIndex < #self._gameData.track.lanes then
				--print("+++++++++ another lane found")
				laneLenghts[laneOffset + 2] = laneLenghts[laneOffset + 2] + self:GetLaneLength(pieceIndex, laneIndex)
				--print("offset " .. laneOffset .. " length " .. self:GetLaneLength(pieceIndex, laneIndex))
			end
		end
	end

	--print("lengths " .. laneLenghts[1] .. ", " .. laneLenghts[2] .. ", " .. laneLenghts[3])

	local bLaneSwitched = false
	if laneLenghts[1] ~= 0 then
		if laneLenghts[1] < laneLenghts[2] then
			self:SwitchLane("Left")
			bLaneSwitched = true
		end
	end
	if not bLaneSwitched and laneLenghts[3] < laneLenghts[2] then
		self:SwitchLane("Right")
	end
end

function NoobBot:GetLaneRadius(_piece, _iLaneIndex)
	local modifier
	if _piece.angle > 0 then
		modifier = 1
	else
		modifier = -1
	end
	return _piece.radius - modifier * self._gameData.track.lanes[_iLaneIndex + 1].distanceFromCenter
end


function NoobBot:GetLaneLength(_iPieceIndex, _iLaneIndex)
	local piece = self._gameData.track.pieces[_iPieceIndex + 1]
	if piece.length then
		return piece.length
	end

	-- compute curve
	local radius = self:GetLaneRadius(piece, _iLaneIndex)
	return radius * math.abs(piece.angle) * 3.14159 / 180
end

function NoobBot:GetAccForSpeed(_speed)
	return _speed / 10
end

function NoobBot:GetSpeedForTarget(_speed, _length)
	-- TODO refine
	return _speed + _length * self.cDecelerationConstant
end

function NoobBot:GetAccForTargetSpeed()
	local speed = self._speedDetector.currentSpeed
	local acc = self._accDetector.currentAcc
	if self._newTargetSpeed ~= self._targetSpeed then
		print("new target speed: "..self._newTargetSpeed)
		self._targetSpeed = self._newTargetSpeed
		self._gotClose = false
	end

	if not self._gotClose then
		if speed < self._targetSpeed then
			if speed + acc * self.cAccAmplitudeFactor > self._targetSpeed then--acc * 
				print("slowbutclose")
				self._gotClose = true
			else
				print("waytooslow")
				self._accToSend = 1
			end
		else
			if speed + acc * self.cDecelAmplitudeFactor < self._targetSpeed then--acc * 
				print("fastbutclose")
				self._gotClose = true
			else
				print("waytoofast")
				self._accToSend = 0
			end
		end
		if self._gotClose then
			self._accToSend = self:GetAccForSpeed(self._targetSpeed)
		end
	end
end

function NoobBot:GetSmart(data, car)
	------------------------------ CONSTANTS TO TWEAK --------------------------
	local cSafeSpeed100					= 6.4
	local cSafeEnterSpeedForRadius100	= cSafeSpeed100 + 2.2--7.9
	local cSafeInterval					= 0.5
	local cCrashAngle100				= 59
	local cTargetDriftAngle				= 50
	local cMaxAngleSpeed				= 6
	local cMaxAllowedAngleSpeed			= 3

	self._lastAngle = car.angle
	local newPieceIndex = car.piecePosition.pieceIndex
	local currentSpeed	= self._speedDetector.currentSpeed

	local angleSpeed
	local angleAbs = math.abs(self._lastAngle)
	if self._lastAngle < 0 and self._prevAngle > 0 or self._lastAngle > 0 and self._prevAngle < 0 then
		angleSpeed = angleAbs + math.abs(self._prevAngle)
	else
		angleSpeed = angleAbs - math.abs(self._prevAngle)
	end

	------------------------- approaching a curve ------------------------
	local numPiecesToLookAhead = 2
	local laneIndex = car.piecePosition.lane.endLaneIndex
	local totalLaneLength = self:GetLaneLength(newPieceIndex, laneIndex) - car.piecePosition.inPieceDistance
	local shouldBreak = false
	for offset = 0, numPiecesToLookAhead do
		local index = (newPieceIndex + offset) % self._numPieces
		local piece = self._gameData.track.pieces[index + 1]
		if not piece.radius then
			if offset ~= 0 then
				totalLaneLength = totalLaneLength + piece.length
			end
		else
			local radius = self:GetLaneRadius(piece, laneIndex)
			-- TODO refine, also take the angle of the car and the angle of the piece
			local safeSpeed = cSafeEnterSpeedForRadius100 * radius / 100
			local safeMaxSpeed = self:GetSpeedForTarget(safeSpeed, totalLaneLength)
			if currentSpeed >= safeMaxSpeed then
				print("breaking, distance "..totalLaneLength.." radius "..radius.." safe speed "..safeSpeed.." safe max speed "..safeMaxSpeed)
				shouldBreak = true
				break
			end

			if offset ~= 0 then
				totalLaneLength = totalLaneLength + self:GetLaneLength(newPieceIndex, laneIndex)
			end
		end
	end

	local allowedAngleSpeed = FloatMap(0, cTargetDriftAngle, angleAbs, cMaxAllowedAngleSpeed, 0)
	local angleAcc = self._prevAngleSpeed - angleSpeed

	local safeAcc = self:GetAccForSpeed(cSafeSpeed100)

	if shouldBreak or self._gameData.track.pieces[newPieceIndex + 1].radius == 100 then
		self._newTargetSpeed = cSafeSpeed100
	else
		print("far away, accelerating")
		self._newTargetSpeed = self.cMaxSpeed
	end

	if math.abs(angleSpeed) > 3 and angleAbs > 20 then
		print("angle speed too high")
		self._newTargetSpeed = self._newTargetSpeed * 0.95
	end

	-- if not shouldBreak and currentSpeed > cSafeSpeed100 and self._gameData.track.pieces[newPieceIndex + 1].radius then
	-- 	------------------------- in a curve ---------------------------------
	-- 	if math.abs(angleSpeed) > allowedAngleSpeed then
	-- 		if angleAbs > cTargetDriftAngle then
	-- 			self._accToSend = 0
	-- 		elseif angleSpeed - allowedAngleSpeed > 2 then
	-- 			self._accToSend = 0
	-- 		else
	-- 			self._accToSend = self:GetAccForSpeed(FloatMap(cSafeSpeed100, 8, currentSpeed, cSafeSpeed100, 0))
	-- 		end
	-- 	else
	-- 		if angleSpeed < self._prevAngleSpeed or angleSpeed < 2 then
	-- 			self._accToSend = self:GetAccForSpeed(FloatMap(0, cCrashAngle100, angleAbs, 10, cSafeSpeed100))
	-- 		else
	-- 			-- nu trebuie zero cand angle speed e putin mai mare decat inainte. mai ales cand e foarte mic
	-- 			self._accToSend = 0
	-- 		end
	-- 	end
	-- end

	------------------------- update stuff -------------------------------
	self:GetAccForTargetSpeed()
	print("angle speed "..angleSpeed..", allowed angle speed "..allowedAngleSpeed)

	-- update current data
	local prevPieceIndex	= self._pieceIndex
	self._pieceIndex		= newPieceIndex
	self._currentLane		= car.piecePosition.lane
	self._prevAngle			= self._lastAngle
	self._prevAngleSpeed	= angleSpeed
	self._prevSpeed			= currentSpeed

	-- switch strategy
	nextPieceIndex = (newPieceIndex + 1) % self._numPieces
	if newPieceIndex ~= prevPieceIndex and self._gameData.track.pieces[nextPieceIndex + 1].switch then
		self:ConsiderSwitch()
	end
end

function NoobBot:Test(data, car)
	local altTarget = self._target - 0.12
	if not self._reached and self._speedDetector.currentSpeed < altTarget then
		return
	end

	if not self._reached and self._speedDetector.currentSpeed >= altTarget then
		self._reached = true
		self._accToSend = self._target / 10
		print("+++++ reached target, speed "..self._speedDetector.currentSpeed)
	end

	self._distFromTarget = self._distFromTarget + self._speedDetector.currentSpeed
	print("dist "..self._distFromTarget)
end

	----------------------- EVENTS ----------------------
function NoobBot:onjoin(data)
	print("Joined")
	self:send(self:ping())
end

function NoobBot:ongameinit(data)
	print("Game init")

	self._gameData	= data.race
	self._numPieces	= #self._gameData.track.pieces
	self._speedDetector.gameData = self._gameData

	-- temp, print for debug
	local gameData = json.encode(data)
	local file = io.open("GameData.json", "w")
	file:write(gameData)
	file:flush()
	file:close()
end

function NoobBot:ongamestart(data)
	print("Race started")
	-- for i = 20, 60 do
	-- 	print(FloatMap(30, 50, i, 1, 0))
	-- end
	-- os.exit(0)
	self:send(self:ping())
end

function NoobBot:oncarpositions(data)
	-- print("OnCarPosition")
	for _, car in ipairs(data) do
		if car.id.name == self.name then
			if self._test then
				self:Test(data, car)
			else
				self:GetSmart(data, car)
			end

			print(">> speed "..self._speedDetector.currentSpeed.." target speed "..self._targetSpeed.." real acc "..self._accDetector.currentAcc.." angle "..self._lastAngle.." piece "..self._pieceIndex.." accelerating to " .. self._accToSend.." <<")

			-- update the speed detector
			self._speedDetector:OnCarPosition(car, self._currentTick)
			self._accDetector:Update()
			break
		end
	end
	self:send(self:throttle(self._accToSend))
end

function NoobBot:oncrash(data)
	print("------------------------Someone crashed------------------------")
	print("Crash speed: " .. self._speedDetector.currentSpeed)
	print("Piece index " .. self._pieceIndex)
	local piece = self._gameData.track.pieces[self._pieceIndex + 1]
	if piece.angle then
		-- get lane radius
		local pieceRadius = piece.radius
		-- to tell whether to add or to substract lane distance from the center
		local modifier
		if piece.angle > 0 then
			modifier = 1
		else
			modifier = -1
		end
		if self._gameData.track.lanes[self._currentLane.startLaneIndex + 1].index ~= self._currentLane.startLaneIndex then
			print("Houston we have a problem - lane index mismatch")
		end
		local laneRadius = piece.radius - modifier * self._gameData.track.lanes[self._currentLane.startLaneIndex + 1].distanceFromCenter
		print("Radius " .. laneRadius)
	end
	print("Car angle " .. self._lastAngle)
	-- self._accToSend = self._accToSend - 0.1
	--print("New acceleration to send is " .. self._accToSend)

	os.exit(1)

	self:send(self:ping())
end

function NoobBot:onturboavailable(data)
	if self._usingTicks then
		self._turboDuration = data.turboDurationTicks
	else
		self._turboDuration = data.turboDurationMilliseconds
	end
	self._turboFactor = data._turboFactor

	print("got turbo of "..self._turboFactor.." for "..self._turboDuration)
end

function NoobBot:onlapfinished(data)
	if data.lapTime.lap == 1 then
		print("lap 2 completed with " .. data.lapTime.millis / 1000)
		os.exit(0)
	end
end

function NoobBot:ongameend(data)
	print("Race ended")
	self:send(self:ping())
end

function NoobBot:onerror(data)
	print("Error: " .. data)
	self:send(self:ping())
end

function NoobBot:msgloop()
	local line = self.conn:receive("*l")
	while line ~= nil and #line > 0 do
	  LOGD("Got message: %s", line)
	  local msg = json.decode.decode(line)
	  if msg then
	  	 self._currentTick = msg.gameTick
		 local msgtype = msg.msgType
		 local fn = NoobBot["on" .. string.lower(msgtype)]
		 if fn then
			fn(self, msg.data)
		 else
			print("Got " .. msgtype)
			self:send(self:ping())
		 end
	  end
	  line = self.conn:receive("*l")
	end
end

if #arg == 4 then
	local host, port, name, key = unpack(arg)
	print("Connecting with parameters:")
	print(string.format("host=%s, port=%s, bot name=%s, key=%s", unpack(arg)))
	local c = assert(socket.connect(host, port))
	bot = NoobBot.create(c, name, key)
	bot:run()
else
	print("Usage: ./run host port botname botkey")
end
